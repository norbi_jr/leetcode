﻿// LT485.cs
using System;
namespace LeetCode
{
    public class LT485
    {
        public static int FindMaxConsecutiveOnes(int[] nums)
        {
            int maxConsecutiveNumber = 0;
            int temp = 0;

            for (int i = 0; i < nums.Length; i++)
            {
                temp = nums[i] == 1 ? temp + 1 : temp;
                maxConsecutiveNumber = temp > maxConsecutiveNumber ? temp : maxConsecutiveNumber;
                temp = nums[i] == 0 ? 0 : temp;
            }

            return maxConsecutiveNumber;
        }
    }
}

