﻿// LT1046.cs
using System;
namespace LeetCode
{
    public class LT1046
    {
        public static int LastStoneWeight(int[] stones)
        {
            List<int> allStones = new();
            for (int i = 0; i < stones.Length; i++)
            {
                allStones.Add(stones[i]);
            }
            int temp = 0;

            while (allStones.Count() > 1)
            {
                allStones = allStones.OrderByDescending(x => x).ToList();
                temp = allStones[0] - allStones[1];
                allStones.Remove(allStones[0]);
                allStones.Remove(allStones[0]);
                if (temp > 0)
                {
                    allStones.Add(temp);
                }
            }
            return allStones.Count() == 1 ? allStones[0] : 0;
        }
    }
}

