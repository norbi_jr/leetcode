﻿// LT557.cs
using System;
namespace LeetCode
{
    public class LT557
    {
        public static string ReverseWords(string s)
        {
            string[] splitted = s.Split(' ');

            for (int i = 0; i < splitted.Length; i++)
            {
                char[] charArr = splitted[i].ToCharArray();
                Array.Reverse(charArr);
                splitted[i] = String.Join("", charArr);
            }

            return String.Join(" ", splitted);
        }
    }
}

