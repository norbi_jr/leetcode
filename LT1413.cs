﻿// LT1413.cs
using System;
namespace LeetCode
{
    public class LT1413
    {
        public static int MinStartValue(int[] nums)
        {
            int startValue = 1;

            int sum = 0;

            int temp = 0;

            while (temp != nums.Length)
            {
                if (temp == 0)
                {
                    sum = startValue + nums[temp];
                }
                else
                {
                    sum += nums[temp];
                }
                temp++;
                if (sum < 1)
                {
                    temp = 0;
                    startValue++;
                }
            }

            return startValue;
        }
    }
}

