CREATE FUNCTION getNthHighestSalary(@N INT) RETURNS INT AS
BEGIN
    RETURN (
        SELECT (SELECT Salary
        FROM Employee
        GROUP BY Salary
        ORDER BY Salary DESC
        OFFSET @N - 1 ROWS
        FETCH NEXT 1 ROWS ONLY
        ))
END