SELECT emp.Name AS Employee
FROM Employee AS emp
LEFT JOIN Employee AS mgr
ON mgr.Id = emp.managerId
WHERE emp.salary > mgr.salary;