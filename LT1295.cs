﻿// LT1295.cs
using System;
namespace LeetCode
{
    public class LT1295
    {
        public static int FindNumbers(int[] nums) => nums.Aggregate(0, (total, num) => num.ToString().Length % 2 == 0 ? total + 1 : total);
    }
}

