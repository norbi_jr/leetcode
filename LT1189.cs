﻿// LT1189.cs
using System;
namespace LeetCode
{
    public class LT1189
    {
        public static int MaxNumberOfBalloons(string text)
        {
            int value = 0;
            var dict = new Dictionary<char, int> { {'b', value }, { 'a', value }, { 'l', value }, { 'o', value }, { 'n', value } };

            foreach (var letter in text)
            {
                if (dict.ContainsKey(letter))
                {
                    dict[letter]++;
                }
            }

            dict['l'] /= 2;
            dict['o'] /= 2;

            return dict.Min(x => x.Value);
        }
    }
}

