﻿// LT506.cs
using System;
namespace LeetCode
{
    public class LT506
    {
        public static string[] FindRelativeRanks(int[] score)
        {
            var sorted = score.OrderByDescending(x => x).ToArray();
            var output = new string[score.Length];
            var dict = new Dictionary<int, string>();

            int rank = 4;
            for (int i = 0; i < sorted.Length; i++)
            {
                if (i == 0)
                {
                    dict.Add(sorted[0], "Gold Medal");
                }
                else if (i == 1)
                {
                    dict.Add(sorted[1], "Silver Medal");
                }
                else if (i == 2)
                {
                    dict.Add(sorted[2], "Bronze Medal");
                }
                else
                {
                    dict.Add(sorted[i], rank.ToString());
                    rank++;
                }
            }

            for (int i = 0; i < score.Length; i++)
            {
                if (dict.ContainsKey(score[i]))
                {
                    output[i] = dict[score[i]];
                }
            }

            return output;
        }
    }
}

