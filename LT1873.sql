SELECT employee_id, 
(CASE
    WHEN name LIKE 'M%' THEN 0
    WHEN employee_id % 2 <> 0 THEN salary
    ELSE 0
END) AS bonus
FROM Employees
ORDER BY employee_id