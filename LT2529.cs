﻿// LT2529.cs
using System;
namespace LeetCode
{
    public class LT2529
    {
        public static int MaximumCount(int[] nums)
        {
            int positive = 0;
            int negative = 0;

            for (int i = 0; i < nums.Length; i++)
            {
                if (nums[i] < 0)
                {
                    negative++;
                }
                else if (nums[i] > 0)
                {
                    positive++;
                }
            }

            return Math.Max(positive, negative);
        }
    }
}

