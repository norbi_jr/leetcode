﻿// LT2160.cs
using System;
namespace LeetCode
{
    public class LT2160
    {
        public static int MinimumSum(int num)
        {
            int[] splittedNum = new int[4];
            for (int i = 0; i < splittedNum.Length; i++)
            {
                splittedNum[i] = num % 10;
                num /= 10;
            }
            Array.Sort(splittedNum);

            return (splittedNum[0] * 10 + splittedNum[2]) + (splittedNum[1] * 10 + splittedNum[3]);
        }
    }
}

