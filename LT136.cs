﻿// LT136.cs
using System;
namespace LeetCode
{
    public class LT136
    {
        public static int SingleNumber(int[] nums)
        {
            Dictionary<int, int> numberDict = new Dictionary<int, int>();
            int value = 1;
            for (int i = 0; i < nums.Length; i++)
            {
                if (numberDict.TryAdd(nums[i], value) == false)
                {
                    numberDict[nums[i]]++;
                }
            }

            int singleNumber = 0;

            foreach (var x in numberDict)
            {
                if (x.Value == 1)
                {
                    singleNumber = x.Key;
                }
            }

            return singleNumber;
        }
    }
}

