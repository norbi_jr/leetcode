﻿// LT521.cs
using System;
namespace LeetCode
{
    public class LT521
    {
        public static int FindLUSlength(string a, string b)
        {
            if (a.Equals(b))
            {
                return -1;
            }
            else
            {
                return Math.Max(a.Length, b.Length);
            }
        }
    }
}