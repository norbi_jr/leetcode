﻿// LT66.cs
using System;
namespace LeetCode
{
    public class LT66
    {
        public static int[] PlusOne(int[] digits)
        {
            int last = digits.Length - 1;

            for (int i = last; i >= 0; i--)
            {
                if (digits[i] < 9)
                {
                    digits[i]++;
                    break;
                }
                else
                {
                    digits[i] = 0;
                }
            }

            if (digits[0] == 0)
            {
                var digitsPlus = new int[last + 2];
                digits.CopyTo(digitsPlus, 1);
                digitsPlus[1] = 0;
                digitsPlus[0] = 1;
                return digitsPlus;
            }

            return digits;
        }
    }
}

