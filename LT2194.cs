﻿// LT2194.cs
using System;
namespace LeetCode
{
    public class LT2194
    {
        public static List<string> CellsInRange(string s)
        {
            List<string> final = new List<string>();

            for (int i = (int)s[0]; i <= (int)s[3]; i++)
            {
                for (int j = s[1]; j <= s[4]; j++)
                {
                    string temp = "";
                    temp += $"{(char)i}{(char)j}";
                    final.Add(temp);
                }
            }

            return final;
        }
    }
}

