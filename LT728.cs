﻿// LT728.cs
using System;
namespace LeetCode
{
    public class LT728
    {
        public static IList<int> SelfDividingNumbers(int left, int right)
        {
            IList<int> output = new List<int>();

            for (int i = left; i <= right; i++)
            {
                bool check = true;
                int temp = i;

                while (temp > 0)
                {
                    int lastDigit = temp % 10;
                    if(lastDigit == 0)
                    {
                        check = false;
                        break;
                    }
                    if (i % lastDigit != 0)
                    {
                        check = false;
                    }
                    temp /= 10;
                }

                if (check)
                {
                    output.Add(i);
                }
            }

            return output;
        }
    }
}

