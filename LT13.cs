﻿// LT13.cs
using System;
namespace LeetCode
{
    public class LT13
    {
        public static int RomanToInt(string s)
        {
            int output = 0;

            Dictionary<char, int> roman = new Dictionary<char, int>();

            roman.Add('M', 1000);
            roman.Add('D', 500);
            roman.Add('C', 100);
            roman.Add('L', 50);
            roman.Add('X', 10);
            roman.Add('V', 5);
            roman.Add('I', 1);

            for (int i = s.Length - 2; i >= 0; i--)
            {
                if (roman.GetValueOrDefault(s[i]) < roman.GetValueOrDefault(s[i + 1]))
                {
                    output -= roman.GetValueOrDefault(s[i]);
                }
                else
                {
                    output += roman.GetValueOrDefault(s[i]);
                }
            }
            output += roman.GetValueOrDefault(s[s.Length - 1]);
            return output;
        }
    }
}

