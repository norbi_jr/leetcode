﻿// LT1480.cs
using System;
namespace LeetCode
{
    public class LT1480
    {
        public static int[] RunningSum(int[] nums)
        {
            for (int i = 1; i < nums.Length; i++)
            {
                nums[i] = nums[i - 1] + nums[i];
            }

            return nums;
        }
    }
}

