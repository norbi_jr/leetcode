﻿// LT1351.cs
using System;
namespace LeetCode
{
    public class LT1351
    {
        public static int CountNegatives(int[][] grid) => grid.SelectMany(x => x).Aggregate(0, (total, number) => number < 0 ? total + 1 : total);
    }
}

