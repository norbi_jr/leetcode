﻿// LT1287.cs
using System;
namespace LeetCode
{
    public class LT1287
    {
        public static int FindSpecialInteger(int[] arr)
        {
            var dict = new Dictionary<int, int>();

            int value = 1;

            for (int i = 0; i < arr.Length; i++)
            {
                if (dict.TryAdd(arr[i], value) == false)
                {
                    dict[arr[i]]++;
                }
            }

            return dict.Where(x => x.Value > arr.Length * 0.25).Select(x => x.Key).FirstOrDefault();
        }
    }
}

