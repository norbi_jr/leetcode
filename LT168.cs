﻿// LT168.cs
using System;
namespace LeetCode
{
    public class LT168
    {
        public static string ConvertToTitle(int columnNumber)
        {
            string final = "";
            while (columnNumber > 0)
            {
                int temp = --columnNumber % 26;
                final += (char)(++temp + 64);
                columnNumber /= 26;
            }
            char[] stringArray = final.ToCharArray();
            string reversedFinal = "";
            for (int i = stringArray.Length - 1; i >= 0; i--)
            {
                reversedFinal += stringArray[i];
            }
            return reversedFinal;
        }
    }
}

