﻿// LT2520.cs
using System;
namespace LeetCode
{
    public class LT2520
    {
        public static int CountDigits(int num)
        {
            int temp = num;
            int output = 0;
            while (temp > 0)
            {
                int number = temp % 10;
                if (num % number == 0)
                {
                    output++;
                }
                temp /= 10;
            }
            return output;
        }
    }
}

