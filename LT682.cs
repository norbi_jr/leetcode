﻿// LT682.cs
using System;
namespace LeetCode
{
    public class LT682
    {
        public static int CalPoints(string[] operations)
        {
            var list = new List<int>();

            for (int i = 0; i < operations.Length; i++)
            {
                switch (operations[i])
                {
                    case "+":
                        list.Add(list.Last() + (list[list.Count - 2]));
                        break;
                    case "D":
                        list.Add(list.Last() * 2);
                        break;
                    case "C":
                        list.RemoveAt(list.Count - 1);
                        break;
                    default:
                        list.Add(Convert.ToInt32(operations[i]));
                        break;
                }
            }

            return list.Aggregate(0, (current, number) => current + number);
        }
    }
}

