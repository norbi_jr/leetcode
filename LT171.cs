﻿// LT171.cs
using System;
namespace LeetCode
{
    public class LT171
    {
        public static int TitleToNumber(string columnTitle)
        {
            int result = 0;
            char[] stringChars = columnTitle.ToCharArray();

            for (int i = 0; i < stringChars.Length; i++)
            {
                result = (result * 26) + stringChars[i] - 64;
            }

            return result;
        }
    }
}

