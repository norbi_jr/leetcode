﻿// LT1844.cs
using System;
namespace LeetCode
{
    public class LT1844
    {
        static char shift(char toConvert, int toMove)
        {
            return (char)(toConvert + (toMove - '0'));
        }

        public static string ReplaceDigits(string s)
        {
            string a = "";
            for (int i = 0; i < s.Length; i++)
            {
                if (i % 2 == 0)
                {
                    a += s[i];
                }
                else
                {
                    a += shift(s[i - 1], s[i]);
                }
            }

            return a;
        }
    }
}

