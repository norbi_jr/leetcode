﻿// LT1200.cs
using System;
namespace LeetCode
{
    public class LT1200
    {
        public static IList<IList<int>> MinimumAbsDifference(int[] arr)
        {
            Array.Sort(arr);

            int difference = arr[arr.Length - 1] - arr[arr.Length - 2];

            IList<IList<int>> list = new List<IList<int>>();

            for (int i = 0; i < arr.Length - 1; i++)
            {
                if (arr[i + 1] - arr[i] < difference)
                {
                    difference = arr[i + 1] - arr[i];
                    list.Clear();
                }
                if (arr[i+1] - arr[i] == difference)
                {
                    IList<int> temp = new List<int>();
                    temp.Add(arr[i]);
                    temp.Add(arr[i + 1]);
                    list.Add(temp);
                }
            }

            return list;
        }
    }
}

