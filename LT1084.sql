SELECT DISTINCT s.product_id, p.product_name
FROM Sales AS s
JOIN Product AS p
ON s.product_id = p.product_id
WHERE p.product_id NOT IN
(
    SELECT p.product_id
    FROM Sales AS s
    JOIN Product AS p
    ON s.product_id = p.product_id
    WHERE s.sale_date NOT BETWEEN '2019-01-01' AND '2019-03-31'
)