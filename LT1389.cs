﻿// LT1389.cs
using System;
namespace LeetCode
{
    public class LT1389
    {
        public static int[] CreateTargetArray(int[] nums, int[] index)
        {
            var list = new List<int>();

            for (int i = 0; i < nums.Length; i++)
            {
                list.Insert(index[i], nums[i]);
            }

            return list.ToArray();
        }
    }
}

