﻿// LT58.cs
using System;
namespace LeetCode
{
    public class LT58
    {
        public static int LengthOfLastWord(string s)
        {
            var splittedS = s.Split(" ");
            for (int i = splittedS.Length - 1; i >= 0; i--)
            {
                if (splittedS[i].Length > 0)
                {
                    return splittedS[i].Length;
                }
            }
            return -1;
        }
    }
}

