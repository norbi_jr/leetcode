﻿// LT9.cs
using System;
namespace LeetCode
{
    public class LT9
    {
        public static bool IsPalindrome(int x)
        {
            if (x < 0)
            {
                return false;
            }
            else if (x < 10)
            {
                return true;
            }
            int temp = x;
            int reversedX = 0;

            while (temp > 0)
            {
                reversedX = reversedX * 10 + temp % 10;
                temp /= 10;
            }
            return x == reversedX;
        }
    }
}

