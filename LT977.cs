﻿// LT977.cs
using System;
namespace LeetCode
{
    public class LT977
    {
        public static int[] SortedSquares(int[] nums) => nums.Select(x => x * x).OrderBy(x => x).ToArray();
    }
}

