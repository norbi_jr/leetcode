﻿// LT1323.cs
using System;
namespace LeetCode
{
    public class LT1323
    {
        public static int Maximum69Number(int num)
        {
            string num1 = num.ToString();

            int final = num;

            for (int i = 0; i < num1.Length; i++)
            {
                string temp = num1;
                if (num1[i] == '6')
                {
                    temp = temp.Remove(i, 1).Insert(i, "9");
                }
                else
                {
                    temp = temp.Remove(i, 1).Insert(i, "6");
                }

                if (Convert.ToInt32(temp) > final)
                {
                    final = Convert.ToInt32(temp);
                }
            }

            return final;
        }
    }
}

