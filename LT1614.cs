﻿// LT1614.cs
using System;
namespace LeetCode
{
    public class LT1614
    {
        public static int MaxDepth(string s)
        {
            var stack = new Stack<char>();

            int counter = 0;

            for (int i = 0; i < s.Length; i++)
            {
                if (s[i] == '(')
                {
                    stack.Push(s[i]);
                }
                else if (s[i] == ')')
                {
                    stack.Pop();
                }
                if (stack.Count > counter)
                {
                    counter = stack.Count;
                }
            }

            return counter;
        }
    }
}

