﻿// LT1859.cs
using System;
namespace LeetCode
{
    public class LT1859
    {
        public static string SortSentence(string s)
        {
            var splitted = s.Split(" ");
            var correctOrder = new string[splitted.Length];

            for (int i = 0; i < splitted.Length; i++)
            {
                correctOrder[splitted[i].Last() - '0' - 1] = splitted[i];
                correctOrder[splitted[i].Last() - '0' - 1] = correctOrder[splitted[i].Last() - '0' - 1].Remove(splitted[i].Length - 1);
            }

            return String.Join(" ", correctOrder);
        }
    }
}

