﻿// LT1154.cs
using System;
namespace LeetCode
{
    public class LT1154
    {
        public static int DayOfYear(string date)
        {
            var dateSplitted = date.Split('-');

            int year = Convert.ToInt32(dateSplitted[0]);
            int month = Convert.ToInt32(dateSplitted[1]);
            int day = Convert.ToInt32(dateSplitted[2]);

            var list = new List<int> { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

            int result = 0;

            result += day;

            for (int i = 0; i < month - 1; i++)
            {
                result += list[i];
            }

            if (year % 4 == 0 && year % 100 != 0 && month > 2)
            {
                result += 1;
            }
            else if (year % 400 == 0 && month > 2)
            {
                result += 1;
            }

            return result;
        }
    }
}

