﻿// LT2418.cs
using System;
namespace LeetCode
{
    public class LT2418
    {
        public static string[] SortPeople(string[] names, int[] heights)
        {
            Dictionary<int, string> dict = new Dictionary<int, string>();
            for (int i = 0; i < names.Length; i++)
            {
                dict.Add(heights[i], names[i]);
            }

            return dict.OrderByDescending(x => x.Key).Select(x => x.Value).ToArray();
        }
    }
}

