﻿// LT520.cs
using System;
namespace LeetCode
{
    public class LT520
    {
        public static bool DetectCapitalUse(string word)
        {
            if (word.ToUpper() == word)
            {
                return true;
            }
            else if (word.ToLower() == word)
            {
                return true;
            }
            else if ((int)word[0] >= 65 && (int)word[0] < 91 && word.Substring(1) == word.Substring(1).ToLower())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

