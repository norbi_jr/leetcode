SELECT a.id AS id
FROM Weather AS a
JOIN Weather AS b
ON DATEDIFF(dd, b.recordDate, a.recordDate) = 1 AND a.temperature > b.temperature;