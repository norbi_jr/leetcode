﻿// LT1184.cs
using System;
namespace LeetCode
{
    public class LT1184
    {
        public static int DistanceBetweenBusStops(int[] distance, int start, int destination)
        {
            if (start > destination)
            {
                int temp = start;
                start = destination;
                destination = temp;
            }

            int sum1 = 0;

            for (int i = start; i < destination; i++)
            {
                sum1 += distance[i];
            }

            int sum2 = 0;

            for (int i = 0; i < start; i++)
            {
                sum2 += distance[i];
            }

            for (int i = distance.Length - 1; i >= destination; i--)
            {
                sum2 += distance[i];
            }

            return Math.Min(sum1, sum2);
        }
    }
}

