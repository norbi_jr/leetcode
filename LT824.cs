﻿// LT824.cs
using System;
namespace LeetCode
{
    public class LT824
    {
        public static string ToGoatLatin(string sentence)
        {
            var output = sentence.Split(" ");

            var vowels = new List<char> { 'a', 'e', 'i', 'o', 'u' };

            string toAdd = "maa";

            for (int i = 0; i < output.Length; i++)
            {
                if (!vowels.Contains(Char.ToLower(output[i][0])))
                {
                    output[i] += output[i][0];
                    output[i] = output[i].Remove(0, 1);
                }
                output[i] += toAdd;
                toAdd += "a";
            }

            return String.Join(" ", output);
        }
    }
}