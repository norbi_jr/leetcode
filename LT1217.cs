﻿// LT1217.cs
using System;
namespace LeetCode
{
    public class LT1217
    {
        public static int MinCostToMoveChips(int[] position)
        {
            int even = 0;
            int odd = 0;

            foreach (var x in position)
            {
                if(x % 2 == 0)
                {
                    even++;
                }
                else
                {
                    odd++;
                }
            }

            return Math.Min(even, odd);
        }
    }
}

