SELECT d.name AS Department, e.name AS Employee, e.salary AS Salary
FROM Employee AS e
JOIN Department AS d
ON d.id = e.departmentId
WHERE e.salary = (SELECT MAX(e1.salary)
FROM Employee AS e1
WHERE e1.departmentId = e.departmentId)