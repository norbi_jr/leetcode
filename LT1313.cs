﻿// LT1313.cs
using System;
namespace LeetCode
{
    public class LT1313
    {
        public static int[] DecompressRLElist(int[] nums)
        {
            var list = new List<int>();

            for (int i = 1; i < nums.Length; i += 2)
            {
                for (int j = 0; j < nums[i - 1]; j++)
                {
                    list.Add(nums[i]);
                }
            }

            return list.ToArray();
        }
    }
}

