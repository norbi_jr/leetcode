﻿// LT1539.cs
using System;
namespace LeetCode
{
    public class LT1539
    {
        public static int FindKthPositive(int[] arr, int k)
        {
            int i = 1;
            while (k > 0)
            {
                if (arr.Contains(i) == false)
                {
                    k--;
                }
                i++;
            }
            return i - 1;
        }
    }
}

