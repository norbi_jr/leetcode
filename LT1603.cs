﻿// LT1603.cs
using System;
namespace LeetCode
{
    public class LT1603
    {
        public int Big { get; set; }
        public int Medium { get; set; }
        public int Small { get; set; }

        public LT1603(int big, int medium, int small)
        {
            Big = big;
            Medium = medium;
            Small = small;
        }

        public bool AddCar(int carType) =>
            carType switch
            {
                1 => !(--Big < 0),
                2 => !(--Medium < 0),
                3 => !(--Small < 0),
                _ => false,
            };
    }
}