﻿// LT1185.cs
using System;
namespace LeetCode
{
    public class LT1185
    {
        public static string DayOfTheWeek(int day, int month, int year) => new DateTime(year, month, day).DayOfWeek.ToString();
    }
}

