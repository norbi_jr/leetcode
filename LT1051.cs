﻿// LT1051.cs
using System;
namespace LeetCode
{
    public class LT1051
    {
        public static int HeightChecker(int[] heights)
        {
            int[] expected = new int[heights.Length];
            Array.Copy(heights, expected, heights.Length);
            Array.Sort(expected);

            int counter = 0;

            for (int i = 0; i < expected.Length; i++)
            {
                if (expected[i] != heights[i])
                {
                    counter++;
                }
            }

            return counter;
        }
    }
}

