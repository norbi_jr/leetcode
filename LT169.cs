﻿// LT169.cs
using System;
namespace LeetCode
{
    public class LT169
    {
        public static int MajorityElement(int[] nums)
        {
            Dictionary<int, int> existNums = new Dictionary<int, int>();
            const int value = 1;

            for (int i = 0; i < nums.Length; i++)
            {
                if (existNums.TryAdd(nums[i], value) == false)
                {
                    existNums[nums[i]]++;
                }
            }

            int calc = nums.Length / 2;

            int result = -997;

            foreach (var x in existNums)
            {
                if (x.Value > calc)
                {
                    result = x.Key;
                }
            }
            return result;
        }
    }
}

