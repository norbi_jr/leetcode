﻿// LT821.cs
using System;
namespace LeetCode
{
    public class LT821
    {
        public static int[] ShortestToChar(string s, char c)
        {
            var output = new int[s.Length];

            int position = 0;

            for (int i = 0; i < output.Length; i++)
            {
                int right = s.IndexOf(c, position);
                int left = s.LastIndexOf(c, position);

                if (right == -1 && left == -1)
                {
                    output[i] = 997;
                }
                else if (left == -1)
                {
                    output[i] = Math.Abs(i - right);
                }
                else if (right == -1)
                {
                    output[i] = Math.Abs(i - left);
                }
                else
                {
                    output[i] = Math.Min(Math.Abs(i - right), Math.Abs(i - left));
                }

                if (output[i] == 0)
                {
                    position = i + 1;
                }
            }

            return output;
        }
    }
}

