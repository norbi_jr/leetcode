SELECT name
FROM SalesPerson
WHERE name NOT IN (
    SELECT sp.name
    FROM SalesPerson AS sp
    LEFT JOIN Orders AS o
    ON sp.sales_id = o.sales_id
    LEFT JOIN Company AS c
    ON c.com_id = o.com_id
    WHERE c.name = 'RED'
)