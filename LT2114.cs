namespace LeetCode;

public class LT2114
{
    public static int MostWordsFound(string[] sentences)
    {
        return sentences.OrderByDescending(x => x.Split(' ').Length).First().Split(' ').Length;
    }
}