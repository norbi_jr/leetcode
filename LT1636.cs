﻿// LT1636.cs
using System;
namespace LeetCode
{
    public class LT1636
    {
        public static int[] FrequencySort(int[] nums)
        {
            var dict = new Dictionary<int, int>();
            const int value = 1;

            for (int i = 0; i < nums.Length; i++)
            {
                if (dict.TryAdd(nums[i], value) == false)
                {
                    dict[nums[i]]++;
                }
            }

            var sorted = dict.OrderBy(x => x.Value).ThenByDescending(x => x.Key);
            int index = 0;

            foreach (var number in sorted)
            {
                for (int i = 0; i < number.Value; i++)
                {
                    nums[index] = number.Key;
                    index++;
                }
            }

            return nums;
        }
    }
}

