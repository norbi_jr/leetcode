﻿// LT1218.cs
using System;
namespace LeetCode
{
    public class LT1281
    {
        public static int SubtractProductAndSum(int n)
        {
            int temp = 0;
            int nSum = 0;
            int nMulti = 1;
            while (n > 0)
            {
                temp = n % 10;
                nSum += temp;
                nMulti *= temp;
                n /= 10;
            }

            return nMulti - nSum;
        }
    }
}

