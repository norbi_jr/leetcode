﻿// LT415.cs
using System;
namespace LeetCode
{
    public class LT415
    {
        public static string AddStrings(string num1, string num2)
        {
            int max = Math.Max(num1.Length, num2.Length);
            int[] output = new int[max];
            int num1Counter = num1.Length - 1;
            int num2Counter = num2.Length - 1;
            int carry = 0;
            for (int i = max - 1; i >= 0; i--)
            {
                int temp = num1[num1Counter] - '0' + num2[num2Counter] - '0' + carry;
                if (temp >= 10)
                {
                    output[i] = temp - 10;
                    carry = 1;
                }
                else
                {
                    output[i] = temp;
                    carry = 0;
                }
                num1Counter--;
                num2Counter--;
            }
            return String.Join("", output);
        }
    }
}

