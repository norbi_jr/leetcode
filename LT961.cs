﻿// LT961.cs
using System;
namespace LeetCode
{
    public class LT961
    {
        public static int RepeatedNTimes(int[] nums)
        {
            var dict = new Dictionary<int, int>();
            const int value = 1;

            for (int i = 0; i < nums.Length; i++)
            {
                if (dict.TryAdd(nums[i], value) == false)
                {
                    return nums[i];
                }
            }

            return 0;
        }
    }
}

