﻿// LT2176.cs
using System;
namespace LeetCode
{
    public class LT2176
    {
        public static int CountPairs(int[] nums, int k)
        {
            int counter = 0;

            for (int i = 0; i < nums.Length; i++)
            {
                for (int j = i + 1; j < nums.Length; j++)
                {
                    int temp = i * j;
                    if (nums[i] == nums[j] && temp % k == 0)
                    {
                        counter++;
                    }
                }
            }

            return counter;
        }
    }
}

