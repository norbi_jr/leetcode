﻿// LT771.cs
using System;
namespace LeetCode
{
    public class LT771
    {
        public static int NumJewelsInStones(string jewels, string stones)
        {
            int counter = 0;

            for (int i = 0; i < stones.Length; i++)
            {
                if (jewels.Contains(stones[i]))
                {
                    counter++;
                }
            }

            return counter;
        }
    }
}

