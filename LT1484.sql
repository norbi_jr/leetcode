SELECT sell_date, COUNT(DISTINCT product) AS num_sold, STRING_AGG(product, ',') WITHIN GROUP (ORDER BY product) AS products
FROM 
(
    SELECT DISTINCT *
    FROM Activities
) AS t
GROUP BY sell_date