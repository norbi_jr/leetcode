﻿// LT1822.cs
using System;
namespace LeetCode
{
    public class LT1822
    {
        static int SignFunc(int x)
        {
            int final = -1;
            if (x > 0)
            {
                final = 1;
            }
            else if (x == 0)
            {
                final = 0;
            }
            return final;
        }

        public static int ArraySign(int[] nums)
        {
            int product = 1;
            for (int i = 0; i < nums.Length; i++)
            {
                product *= nums[i];
            }

            return SignFunc(product);
        }
    }
}

