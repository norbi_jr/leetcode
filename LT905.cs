﻿// LT905.cs
using System;
namespace LeetCode
{
    public class LT905
    {
        public static int[] SortArrayByParity(int[] nums)
        {
            for (int i = 0; i < nums.Length; i++)
            {
                if (nums[i] % 2 != 0)
                {
                    for (int j = i; j < nums.Length; j++)
                    {
                        if (nums[j] % 2 == 0)
                        {
                            int temp = nums[i];
                            nums[i] = nums[j];
                            nums[j] = temp;
                        }
                    }
                }
            }

            return nums;
        }
    }
}

