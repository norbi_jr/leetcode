﻿// LT509.cs
using System;
namespace LeetCode
{
    public class LT509
    {
        public static int Fib(int n)
        {
            if (n <= 1)
            {
                return n;
            }

            return Fib(n - 1) + Fib(n - 2);
        }
    }
}

