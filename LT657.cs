﻿// LT657.cs
using System;
namespace LeetCode
{
    public class LT657
    {
        public static bool JudgeCircle(string moves)
        {
            int[] position = { 0, 0 };  // [0] - Up/Down    [1] - Left/Right

            for (int i = 0; i < moves.Length; i++)
            {
                if (moves[i] == 'U')
                {
                    position[0]++;
                }
                else if (moves[i] == 'D')
                {
                    position[0]--;
                }
                else if (moves[i] == 'L')
                {
                    position[1]--;
                }
                else
                {
                    position[1]++;
                }
            }

            return position[0] == 0 && position[1] == 0;
        }
    }
}

