﻿// LT628.cs
using System;
namespace LeetCode
{
    public class LT628
    {
        public static int MaximumProduct(int[] nums)
        {
            Array.Sort(nums);
            int numCount = nums.Length;
            return Math.Max(nums[0] * nums[1] * nums[numCount - 1], nums[numCount - 1] * nums[numCount - 2] * nums[numCount - 3]);
        }
    }
}

