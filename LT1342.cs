﻿// LT1342.cs
using System;
namespace LeetCode
{
    public class LT1342
    {
        public static int NumberOfSteps(int num)
        {
            int counter = 0;
            while (num != 0)
            {
                if (num % 2 == 0)
                {
                    num /= 2;
                }
                else
                {
                    num -= 1;
                }
                counter++;
            }

            return counter;
        }
    }
}

