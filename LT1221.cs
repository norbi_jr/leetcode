﻿// LT1221.cs
using System;
namespace LeetCode
{
    public class LT1221
    {
        public static int BalancedStringSplit(string s)
        {
            var stack = new Stack<char>();
            int counter = 0;

            for (int i = 0; i < s.Length; i++)
            {
                if (stack.Count != 0 && stack.Peek() != s[i])
                {
                    stack.Pop();
                }
                else
                {
                    stack.Push(s[i]);
                }
                if (stack.Count == 0)
                {
                    counter++;
                }
            }

            return counter;
        }
    }
}

