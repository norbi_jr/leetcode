﻿// LT929.cs
using System;
namespace LeetCode
{
    public class LT929
    {
        public static int NumUniqueEmails(string[] emails)
        {
            var dict = new Dictionary<string, int>();
            const int counter = 1;

            for (int i = 0; i < emails.Length; i++)
            {
                int lengthOfAlias = emails[i].IndexOf('@') - emails[i].IndexOf('+');

                for (int j = 0; j < emails[i].IndexOf('@'); j++)
                {
                    if (emails[i][j] == '.')
                    {
                        emails[i] = emails[i].Remove(j, 1);
                    }
                    if (emails[i][j] == '+')
                    {
                        emails[i] = emails[i].Remove(j, lengthOfAlias);
                    }
                }
                dict.TryAdd(emails[i], counter);
            }

            return dict.Count;
        }
    }
}

