﻿// LT748.cs
using System;
namespace LeetCode
{
    public class LT748
    {
        public static string ShortestCompletingWord(string licensePlate, string[] words)
        {
            licensePlate = licensePlate.ToLower();

            string converted = String.Concat(licensePlate.Where(x => x >= 'a' && x <= 'z'));

            var completedWords = new List<string>();

            for (int i = 0; i < words.Length; i++)
            {
                var convertedRepeated = new String(converted);

                for (int j = 0; j < words[i].Length; j++)
                {
                    if (convertedRepeated.Contains(words[i][j]))
                    {
                        int number = convertedRepeated.IndexOf(words[i][j]);
                        convertedRepeated = convertedRepeated.Remove(number, 1);
                    }
                }

                if (String.IsNullOrWhiteSpace(convertedRepeated))
                {
                    completedWords.Add(words[i]);
                }
            }

            return completedWords.Aggregate( (first, second) => first.Length > second.Length ? second : first);
        }
    }
}

