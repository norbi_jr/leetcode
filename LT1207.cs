﻿// LT1207.cs
using System;
namespace LeetCode
{
    public class LT1207
    {
        public static bool UniqueOccurrences(int[] arr)
        {
            var dict = new Dictionary<int, int>();
            var value = 1;

            for (int i = 0; i < arr.Length; i++)
            {
                if (dict.TryAdd(arr[i], value) is false)
                {
                    dict[arr[i]]++;
                }
            }

            var finalDict = new Dictionary<int, int>();

            for (int i = 0; i < dict.Count; i++)
            {
                if (finalDict.TryAdd(dict.ElementAt(i).Value, value) is false)
                {
                    return false;
                }
            }

            return true;
        }
    }
}

