﻿// LT1331.cs
using System;
namespace LeetCode
{
    public class LT1331
    {
        public static int[] ArrayRankTransform(int[] arr)
        {
            int value = 1;
            var sortedArray = arr.OrderBy(x => x);

            var sortedDict = new Dictionary<int, int>();

            foreach (var item in sortedArray)
            {
                if (!sortedDict.ContainsKey(item))
                {
                    sortedDict.Add(item, value);
                    value++;
                }
            }

            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = sortedDict.GetValueOrDefault(arr[i]);
            }

            return arr;
        }
    }
}

