﻿// LT1370.cs
using System;
namespace LeetCode
{
    public class LT1370
    {
        public static string SortString(string s)
        {
            Dictionary<char, int> dict = new Dictionary<char, int>();

            int value = 1;

            for (int i = 0; i < s.Length; i++)
            {
                if (dict.TryAdd(s[i], value) == false)
                {
                    dict[s[i]]++;
                }
            }
            var sortedDict = dict.OrderBy(x => x.Key).ToDictionary(x => x.Key, y => y.Value);

            string output = "";

            while (sortedDict.Sum(x => x.Value) > 0)
            {
                for (int i = 0; i < sortedDict.Count(); i++)
                {
                    if (sortedDict.ElementAt(i).Value > 0)
                    {
                        output += sortedDict.ElementAt(i).Key;
                        sortedDict[sortedDict.ElementAt(i).Key]--;
                    }
                }
                for (int i = sortedDict.Count() - 1; i >= 0; i--)
                {
                    if (sortedDict.ElementAt(i).Value > 0)
                    {
                        output += sortedDict.ElementAt(i).Key;
                        sortedDict[sortedDict.ElementAt(i).Key]--;
                    }
                }
            }
            return output;
        }
    }
}

